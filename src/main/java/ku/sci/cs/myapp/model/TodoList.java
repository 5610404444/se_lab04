package ku.sci.cs.myapp.model;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TodoList {
	
	private String date; 
	private String description; 
	private String status;
	private SimpleDateFormat dateinformat;
	private Date datein; 
	
	public TodoList(String aDate,String aDescription,String status) throws ParseException{
		dateinformat = new SimpleDateFormat("dd/MM/yy");
		datein = dateinformat.parse(aDate);
		date = aDate;
		description = aDescription;
		status = "none";
		
	
	}
	
	public String toString(){ 
		return "Task[ date="+date+" ,description="+description+",Done="+status+"]";
	}
	
	
	
	public boolean occursOn(String aDate) throws ParseException {
		
		DateFormat compare = new SimpleDateFormat("dd/MM/yy");
		Date compareParse  = compare.parse(aDate);
		
		if(datein.getDate()==compareParse.getDate()&&datein.getMonth()==compareParse.getMonth()&&datein.getYear()==compareParse.getYear()){ 
			return true;
		}
		else { 
			return false;
		}
		
		
	}
	
	public void setStatus(boolean up,String aStatus){ 
		
		if(up==true){ 
			status = aStatus;
		}
		
	}

	public String getDesc() {
		return description;
	}

	public String getDate() {
		return date;
	}

	public String getStatus() {
		return status;
	}



	
}
