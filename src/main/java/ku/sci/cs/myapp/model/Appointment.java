package ku.sci.cs.myapp.model;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


/**Autor : Narumon Petsiri 5610404444*/

public class Appointment {

	protected String description; 
	protected Date datein; 
	protected DateFormat dateinformat;
	protected String date; 
	protected String type;
	protected String result = "";
	
	
	
	public Appointment(String aDate,String aDescription, String aType) throws ParseException{
		
		dateinformat = new SimpleDateFormat("dd/MM/yy HH:mm");
		datein = dateinformat.parse(aDate);
		date = aDate;
		description = aDescription; 
		type = aType;
		
	}
	
	public String toString(){ 
		return "Appointment[ date="+date+" ,description="+description+" ,type="+type+"]";
	}
	
	public boolean occursOn(String aDate) throws ParseException { 
		return false; 
	}

	public String getDesc() {
		// TODO Auto-generated method stub
		return description;
	}

	public String getDate() {
		// TODO Auto-generated method stub
		return date;
	}
	
	

}
