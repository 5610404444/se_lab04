package ku.sci.cs.myapp.model;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;


/**Autor : Narumon Petsiri 5610404444*/

public class AppointmentBook {
	
	protected List<Appointment> appointBook;
	protected List<TodoList> taskBook;
	private int numAppointment;
	private int numTask; 
	private String displayData;
	protected String yourDate; 
	
	public  AppointmentBook(){
		appointBook = new ArrayList<Appointment>();
		taskBook = new ArrayList<TodoList>();
	}
	
	
	//<ADD APPOINTMENT BOOK>
	public void addAllAppointment(Appointment list){
		appointBook.add(list);
		
	}
	public void addAppointment(Object apt){
		appointBook.add((Appointment) apt);
	}
	public void addAppointment(String date,String desc,String type) throws ParseException{
		
		if(type.equalsIgnoreCase("Onetime")){
			
			Appointment apt = new Onetime(date, desc,type);
			appointBook.add(apt);
		}
		
		else if(type.equalsIgnoreCase("Daily")){
			
			Appointment apt = new Daily(date, desc,type);
			appointBook.add(apt);
		}
		else if(type.equalsIgnoreCase("Weekly")){
			
			Appointment apt = new Weekly(date, desc,type);
			appointBook.add(apt);
		}
		else if(type.equalsIgnoreCase("Monthly")){
			
			Appointment apt = new Monthly(date, desc,type);
			appointBook.add(apt);
		}
		
		
	}
	
	
	//<ADD TASK BOOK>
	public void addAllTask(TodoList list){
		taskBook.add(list);
		
	}
	public void addTask(Object apt){
		taskBook.add((TodoList) apt);
	}
	public void addTask(String date,String desc) throws ParseException{
		
		TodoList apt = new TodoList(date, desc,"-");
		taskBook.add(apt);
	}
	
	//< GET VALUE>
	public int getNumofAppointTask(){
		numAppointment = appointBook.size()+taskBook.size();
		return numAppointment ;
	}
	
	
	public int getNumofappoint(){ 
		numAppointment = appointBook.size();
		return numAppointment;
	}
	
	public int getNumoftask(){ 
		numTask = taskBook.size();
		return numTask;
	}

	
	//<GET BY INDEX>
	public String getAppoint(int index){
		return appointBook.get(index-1).toString();
	}
	
	public String getTask(int index){ 
		return taskBook.get(index-1).toString();
	}
	
	
	//<CHECK APPOINTMENT AND TASK>
	public String checkDate(String aDate,String aType) throws ParseException{ 
		String hasDate = "";
		
		if(aType.equalsIgnoreCase("A")){
			for(int i = 0 ; i<=appointBook.size()-1;i++){ 
				if(appointBook.get(i).occursOn(aDate)==true){
					hasDate = appointBook.get(i).toString()+"\n";
				}
			}
		}
		
		else if (aType.equalsIgnoreCase("T")){ 
			
			for(int i = 0 ; i<=taskBook.size()-1;i++){ 
				if(taskBook.get(i).occursOn(aDate)==true){
					hasDate = taskBook.get(i).toString()+"\n";
				}
			}
			
		}
		else if (aType.equalsIgnoreCase("B")){ 
			
			for(int i = 0 ; i<=appointBook.size()-1;i++){ 
				if(appointBook.get(i).occursOn(aDate)==true){
					hasDate += appointBook.get(i).toString()+"\n";
				}
			}
			for(int i = 0 ; i<=taskBook.size()-1;i++){ 
				if(taskBook.get(i).occursOn(aDate)==true){
					hasDate += taskBook.get(i).toString()+"\n";
				}
			}
			
		}
		
		return hasDate;
		
	}
	
	//<UPDATE>
	
	public boolean update(String aDate,String status) throws ParseException{
		
		boolean canup = false;
		for(int i = 0 ; i<=taskBook.size()-1;i++){ 
			if(taskBook.get(i).occursOn(aDate)==true){
				canup =  true; 
				taskBook.get(i).setStatus(canup, status);
			}
			else {
				canup =  false;
			}
		}
		
		return canup;
		
	}
	
	
	public String toString(String type){
		displayData = "";
		if(type.equalsIgnoreCase("A")){ 
			for(int i = 0; i<appointBook.size();i++){ 
				displayData +=appointBook.get(i).toString()+"\n";
			}
		}
		else if(type.equalsIgnoreCase("T")){ 
			for(int i = 0; i<taskBook.size();i++){ 
				displayData +=taskBook.get(i).toString()+"\n";
			}
		}
		
		else if(type.equalsIgnoreCase("B")){ 
			for(int i = 0; i<appointBook.size();i++){ 
				displayData +=appointBook.get(i).toString()+"\n";
			}
			for(int i = 0; i<taskBook.size();i++){ 
				displayData +=taskBook.get(i).toString()+"\n";
			}
		}
		return displayData; 
	}
	

}
