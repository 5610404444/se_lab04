package ku.sci.cs.myapp.model;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Onetime extends Appointment {
	
	
	public Onetime(String date, String desc, String type) throws ParseException {
		super(date,desc,type);
	}
	@Override
	public boolean occursOn(String aDate) throws ParseException {
		
		DateFormat compare = new SimpleDateFormat("dd/MM/yy");
		Date compareParse  = compare.parse(aDate);
		
		
		
		if(super.datein.getDate()==compareParse.getDate()&&super.datein.getMonth()==compareParse.getMonth()&&super.datein.getYear()==compareParse.getYear()){ 
			return true;
		}
		else { 
			return false;
		}
		
		
	}
	
}
