package ku.sci.cs.myapp.controller;
import java.io.IOException;
import java.text.ParseException;

import ku.sci.cs.myapp.model.AppointmentBook;
import ku.sci.cs.myapp.view.AppointmentConsole;



/**Autor : Narumon Petsiri 5610404444*/


public class AppointmentMain {

	public static void main(String[] args) throws IOException, ParseException {
		AppointmentBook appointBook = new AppointmentBook();
		AppointmentConsole appointConsole = new AppointmentConsole(appointBook);
		appointConsole.start();

	}

}
