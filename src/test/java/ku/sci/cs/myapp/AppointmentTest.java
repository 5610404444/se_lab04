package ku.sci.cs.myapp;

import static org.junit.Assert.*;

import java.io.IOException;
import java.text.ParseException;

import ku.sci.cs.myapp.model.Appointment;
import ku.sci.cs.myapp.model.AppointmentBook;

import org.junit.Test;

public class AppointmentTest {
	
	@Test
	public void appointmentTestTrue() throws ParseException {
		Appointment appoint = new Appointment("9/04/2015 10:14","JTest","Onetime");
		assertEquals("JTest", appoint.getDesc());
		assertEquals("9/04/2015 10:14", appoint.getDate());
	}
	
	@Test(expected=AssertionError.class)
	public void appointmentTestFlase() throws ParseException,AssertionError {
		Appointment appoint = new Appointment("9/04/2015 10:14","JTest","Onetime");
		assertEquals("JTest", appoint.getDesc());
		assertEquals("9/04/2015 14:14", appoint.getDate());
	}
	
	

}
