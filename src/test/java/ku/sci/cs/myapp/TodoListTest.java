package ku.sci.cs.myapp;

import static org.junit.Assert.*;
import java.text.ParseException;
import ku.sci.cs.myapp.model.TodoList;
import org.junit.Test;

public class TodoListTest {

	@Test
	public void todoListTestTrue() throws ParseException {
		TodoList task = new TodoList("9/04/2015","task1","none");
		assertEquals("9/04/2015", task.getDate());
		assertEquals("task1", task.getDesc());
		
		
	}
	
	@Test(expected=AssertionError.class)
	public void todoListTestFalse() throws ParseException,AssertionError {
		TodoList task = new TodoList("9/04/2015","task1","-");
		assertEquals("9/04/2015", task.getDate());
		assertEquals("task3", task.getDesc());
		
	}
	

}
